package com.codenjoy.dojo.snake.client;

import com.codenjoy.dojo.client.Solver;
import com.codenjoy.dojo.client.WebSocketRunner;
import com.codenjoy.dojo.services.Direction;

public class YourSolver implements Solver<Board> {

  @Override
  public String get(Board board) {
    SmartSnake ss = new SmartSnake(board);
    Direction solved = ss.solve();
    return solved.toString();
  }

  public static void main(String[] args) {
    WebSocketRunner.runClient(

            //"http://167.71.44.71/codenjoy-contest/board/player/3edq63tw0bq4w4iem7nb?code=12345678901234567890",
           "http://159.89.27.106/codenjoy-contest/board/player/pa34cnticjvmbedragtz?code=1233759459653362282",
    new YourSolver(),
        new Board()
    );
  }
}
